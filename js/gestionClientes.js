var clientesObtenidos;

function getClientes() {
 var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
 var request = new XMLHttpRequest();
 request.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200) {
       console.log(request.responseText);
       clientesObtenidos = request.responseText;
       procesarClientes();
     }
   }
  request.open("GET", url, true);
  request.send();
}


function procesarClientes()
{
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var extensionPng = ".png";
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla = document.getElementById("tablaClientes");
  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaTelefono = document.createElement("td");
    columnaTelefono.innerText = JSONClientes.value[i].Phone;

    var columnaPais = document.createElement("td");
    var imgPais = document.createElement("img");

    if (JSONClientes.value[i].Country == "UK"){
        imgPais.src = rutaBandera + "United-Kingdom" + extensionPng;
    } else {
        imgPais.src = rutaBandera + JSONClientes.value[i].Country + extensionPng;
    }

    imgPais.classList.add("flag");
    
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaTelefono);
    columnaPais.appendChild(imgPais);
    nuevaFila.appendChild(columnaPais);

    tabla.appendChild(nuevaFila);
  }
}
